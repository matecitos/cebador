### Up

```shell
export COMPOSE_FILE=compose/stages/dev/docker-compose.yml
docker-compose up -d
```

### Super user

```shell
docker-compose exec web bash
cd cebador/
python manage.py createsuperuser
```

### Visits

- [status](http://localhost:8000/ht/)  
- [flower](http://localhost:8888/)  
- [rabbitmq](http://localhost:15672/#/)  
- [django admin](http://localhost:8000/admin)  
- [grafana](http://localhost:3001)  
- [prometheus](http://localhost:9090)  

### Add tasks

- [http://localhost:8000/api/fast/20](http://localhost:8000/api/fast/20) adds 20 fast tasks  
- [http://localhost:8000/api/slow/2](http://localhost:8000/api/slow/2) adds 2 slow tasks  

### Auto Scaling

Autoscaling use the requirements inside `scale/` and should be installed inside a venv (or the base system)

- `queue_chequer` read the given queue name total messages and messages ready to be processed
- `scaler` calls `docker-compose` commands
- `auto_scaler` uses the previous services and apply a logic to scale up or turn off workers

```shell
python compose/scale/auto_scaler.py
```

then add tasks with the given API calls. Watch Flower, RabbitMQ, Grafana and the `auto_scaler` output to see the magic

### Manually disable workers

```shell
docker-compose exec worker-slow-drinker bash

# sh entrypoints/worker_check.sh  #  To check if the worker has any tasks on going

# sh entrypoints/worker_deactivate.sh  #  To stop worker for consuming tasks of the queue

# sh entrypoints/worker_shutdown.sh  #  To shutdown worker and with it, the whole container will stop
```

### TODO

- ATM the workers are scaled down when the queue is empty rather than when it is idle, and that is not performant.
- Make `worker_check.sh` returns a usable value instead of celery output (can use grep)
- Prepare prod environment `prod/docker-compose.yml`
- Add Makefile to made calls easier
- Stop using `pyrabbit` and use bare python to call rabbitmq API or directly from Prometheus
