from pyrabbit.api import Client


class QueueChecker:
    _DEFAULT_VHOST = "/"

    def __init__(self, host: str, user: str, password: str):
        self._client = Client(host, user, password)

    def get_queue_ready_messages(self, queue_name: str) -> int:
        queue = self._client.get_queue(self._DEFAULT_VHOST, queue_name)
        return queue["messages_ready"]

    def get_queue_length(self, queue_name: str) -> int:
        return self._client.get_queue_depth(self._DEFAULT_VHOST, queue_name)
