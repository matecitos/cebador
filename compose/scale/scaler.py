import os


class Scaler:

    DOCKER_COMPOSE_FILE_LOCATION = "compose/stages/dev/docker-compose.yml"
    BASE_COMMAND = f"docker-compose -f {DOCKER_COMPOSE_FILE_LOCATION}"
    # hardcoded initial scale
    INITIAL_SETUP = f"{BASE_COMMAND} up -d --scale worker-fast-drinker=1 --scale worker-slow-drinker=1"

    def is_infra_up(self) -> bool:
        command = f'{self.BASE_COMMAND} ps --filter "status=running" --services | wc -l'
        return int(os.popen(command).read()) > 1

    def count_available_workers(self, worker_name: str) -> int:
        return int(os.popen(f"{self.BASE_COMMAND} ps | grep -c {worker_name}").read())

    def start_infra(self):
        os.system(self.INITIAL_SETUP)

    def scale_up_workers(self, worker_name: str, desired_scaling: int):
        available_workers = self.count_available_workers(worker_name)
        assert (
            desired_scaling > available_workers,
            f"Desired scaling {desired_scaling} is less than available workers {available_workers}"
        )

        service_name = f"worker-{worker_name}"
        os.system(
            f"{self.BASE_COMMAND} up -d --scale {service_name}={desired_scaling} {service_name}"
        )

    def scale_down_workers(self, worker_name: str, desired_scaling: int):
        service_name = f"worker-{worker_name}"
        os.system(
            f"{self.BASE_COMMAND} up -d --scale {service_name}={desired_scaling} {service_name}"
        )
