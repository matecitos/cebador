import math
from pprint import pprint
from time import sleep

from scaler import Scaler

from queue_chequer import QueueChecker

fast_drinker_queue_name = "fast-drinker"
slow_drinker_queue_name = "slow-drinker"

config = {
    slow_drinker_queue_name: dict(
        min_workers=1,
        max_workers=10,
        messages_threshold=2
    ),
    fast_drinker_queue_name: dict(
        min_workers=1,
        max_workers=5,
        messages_threshold=10
    )
}


class AutoScaler:

    def __init__(self, scaler: Scaler, queue_checker: QueueChecker):
        self._scaler = scaler
        self._queue_checker = queue_checker

    def run_initial(self):
        if self._scaler.is_infra_up():
            print(f"Infra is up")
        else:
            print(f"Starting infra")
            self._scaler.start_infra()

    def run_worker_asg(self, worker_name: str):
        try:
            max_workers = config.get(worker_name).get("max_workers")
            min_workers = config.get(worker_name).get("min_workers")
            messages_threshold = config.get(worker_name).get("messages_threshold")
            queue_length = self._queue_checker.get_queue_length(worker_name)
            ready_messages = self._queue_checker.get_queue_ready_messages(worker_name)
            available_workers = self._scaler.count_available_workers(worker_name)

            required_workers = max(math.ceil(queue_length / messages_threshold), min_workers)
            desired_scaling = max_workers if required_workers > max_workers else required_workers

            results = dict(
                queue_length=queue_length,
                ready_messages=ready_messages,
                available_workers=available_workers,
                required_workers=required_workers,
                desired_scaling=desired_scaling
            )
            pprint(f'{"-" * 5} Checking {worker_name} {"-" * 5}')
            print(results)

            self._apply_asg(
                worker_name=worker_name, available_workers=available_workers,
                queue_length=queue_length, desired_scaling=desired_scaling
            )
        except Exception as ex:
            pprint(ex)

    def _apply_asg(
            self, worker_name: str, available_workers: int, queue_length: int, desired_scaling: int
    ):
        min_workers = config.get(worker_name).get("min_workers")
        # Turning of workers
        if available_workers > min_workers and queue_length == 0:
            self._scaler.scale_down_workers(worker_name, min_workers)

        # Scaling worker
        if desired_scaling > available_workers:
            self._scaler.scale_up_workers(worker_name, desired_scaling)


if __name__ == "__main__":
    scaler = Scaler()
    queue_checker = QueueChecker('localhost:15672', 'user', 'password')
    lambda_service = AutoScaler(scaler=scaler, queue_checker=queue_checker)

    lambda_service.run_initial()

    sleep(2)
    while True:
        sleep(5)
        lambda_service.run_worker_asg(slow_drinker_queue_name)
        lambda_service.run_worker_asg(fast_drinker_queue_name)
