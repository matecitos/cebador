#!/bin/sh
cd cebador
python manage.py migrate
python3 manage.py collectstatic --no-input --clear
python manage.py runserver 0.0.0.0:8000

exec tail -f /dev/null