#!/bin/sh
cd cebador
celery -A ${CELERY_APP} control cancel_consumer ${WORKER_QUEUE} -d celery@${WORKER_QUEUE}-${HOSTNAME}
