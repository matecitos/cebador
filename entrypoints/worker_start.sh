#!/bin/sh
cd cebador
celery -A ${CELERY_APP} worker \
-Q ${WORKER_QUEUE} -n ${WORKER_QUEUE}-${HOSTNAME} \
--concurrency=${CONCURRENCY} --prefetch-multiplier=1 \
--loglevel=INFO \
--task-events --without-gossip --without-mingle


# celery -A ${CELERY_APP} inspect active -d celery@${WORKER_QUEUE}-${HOSTNAME}
# celery -A ${CELERY_APP} control cancel_consumer ${WORKER_QUEUE} -d celery@${WORKER_QUEUE}-${HOSTNAME}
# celery -A ${CELERY_APP} control shutdown -d celery@${WORKER_QUEUE}-${HOSTNAME}
