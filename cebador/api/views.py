from rest_framework.response import Response
from rest_framework.views import APIView

from api.models import Drinker, DrinkerSpeeds, Round
from api.tasks import ServeFastDrinker, ServeSlowDrinker


class SpawnSlowDrinkers(APIView):
    def get(self, request, quantity: int):
        round = Round.objects.create()
        for _ in range(quantity):
            drinker = Drinker.objects.create(round=round, speed=DrinkerSpeeds.SLOW)
            ServeSlowDrinker().apply_async(kwargs=dict(drinker_id=drinker.id))
        return Response(data=dict(queued=quantity))


class SpawnFastDrinkers(APIView):
    def get(self, request, quantity: int):
        round = Round.objects.create()
        for _ in range(quantity):
            drinker = Drinker.objects.create(round=round, speed=DrinkerSpeeds.FAST)
            ServeFastDrinker().apply_async(kwargs=dict(drinker_id=drinker.id))
        return Response(data=dict(queued=quantity))
