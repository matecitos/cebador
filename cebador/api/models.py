from enum import unique, Enum
from uuid import uuid4

from django.db import models
from enumfields import EnumField


class Round(models.Model):
    uuid = models.UUIDField(primary_key=True, default=uuid4, editable=False)
    created_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return f"{str(self.uuid)} - {self.created_at:%Y-%m-%d %H:%M:%S.%f)}"


@unique
class DrinkerSpeeds(Enum):
    FAST = "FAST"
    SLOW = "SLOW"


class Drinker(models.Model):
    round = models.ForeignKey(Round, on_delete=models.CASCADE, related_name="drinkers")
    speed = EnumField(DrinkerSpeeds, max_length=50)
    created_at = models.DateTimeField(auto_now_add=True)
    served_at = models.DateTimeField(null=True, blank=True)

    @property
    def waited(self):
        if self.served_at is None:
            return None

        return (self.served_at - self.created_at).seconds
