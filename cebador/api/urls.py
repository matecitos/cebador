from django.urls import path

from . import views

app_name = 'api'
urlpatterns = [
    path('fast/<int:quantity>', views.SpawnFastDrinkers.as_view(), name='fast'),
    path('slow/<int:quantity>', views.SpawnSlowDrinkers.as_view(), name='slow'),
]

