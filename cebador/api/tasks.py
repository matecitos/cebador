import logging
from datetime import datetime
from time import sleep
from typing import Optional

from django.conf import settings
from celery.task import Task

from api.models import Drinker, DrinkerSpeeds
from cebador.celery import Queues

logger = logging.getLogger(__name__)

FAST_DRINKER_TIME_SECONDS = settings.FAST_DRINKER_TIME_SECONDS
SLOW_DRINKER_TIME_SECONDS = settings.SLOW_DRINKER_TIME_SECONDS


class ServeSlowDrinker(Task):
    queue = Queues.slow_drinker.value
    acks_late = True
    reject_on_worker_lost = True
    time_limit = SLOW_DRINKER_TIME_SECONDS * 2

    def run(self, drinker_id: int, *args, **kwargs):
        logger.info(f"Drinking slow for {drinker_id}")
        drinker = Drinker.objects.get(id=drinker_id)
        sleep(SLOW_DRINKER_TIME_SECONDS)
        drinker.served_at = datetime.now()
        drinker.save()


class ServeFastDrinker(Task):
    queue = Queues.fast_drinker.value
    acks_late = True
    reject_on_worker_lost = True
    time_limit = FAST_DRINKER_TIME_SECONDS * 2

    def run(self, drinker_id: int, *args, **kwargs):
        logger.info(f"Drinking fast for {drinker_id}")
        drinker = Drinker.objects.get(id=drinker_id)
        sleep(FAST_DRINKER_TIME_SECONDS)
        drinker.served_at = datetime.now()
        drinker.save()


class ScheduleMates(Task):
    queue = Queues.fast_drinker.value

    def run(self, speed: Optional[DrinkerSpeeds] = None, *args, **kwargs):
        drinkers = Drinker.objects.filter(served_at__isnull=True)
        if speed is not None:
            drinkers = drinkers.filter(speed=speed)

        for drinker in drinkers.iterator():
            if drinker.speed == DrinkerSpeeds.SLOW:
                ServeSlowDrinker().apply_async(kwargs=dict(drinker_id=drinker.id))
            if drinker.speed == DrinkerSpeeds.FAST:
                ServeFastDrinker().apply_async(kwargs=dict(drinker_id=drinker.id))
