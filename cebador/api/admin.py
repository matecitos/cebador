from django.contrib import admin
from api.models import Drinker, Round


@admin.register(Round)
class RoundAdmin(admin.ModelAdmin):
    list_display = [
        'uuid', 'created_at_detailed', 'duration', 'drinkers_count', 'unserved_count', 'speed'
    ]
    ordering = ['-created_at']

    def created_at_detailed(self, obj: Round):
        return obj.created_at.strftime("%d %b %Y %H:%M:%S.%f")[:-3]

    def drinkers_count(self, obj: Round):
        return obj.drinkers.count()

    def unserved_count(self, obj: Round):
        return obj.drinkers.filter(served_at__isnull=True).count()

    def duration(self, obj: Round):
        drinkers = obj.drinkers.all()

        if not drinkers:
            return None

        last_served = max(d.served_at for d in drinkers if d.served_at is not None)

        if last_served is None:
            return None

        return f"{(last_served - obj.created_at).seconds}s"

    def speed(self, obj: Round):
        drink = obj.drinkers.first()
        if drink is None:
            return None

        return drink.speed.value


@admin.register(Drinker)
class DrinkerAdmin(admin.ModelAdmin):
    list_display = [
        'round_uuid', 'id', 'speed', 'created_at_detailed', 'served_at_detailed', 'waited_seconds'
    ]
    list_filter = ['speed', 'served_at']
    sortable_by = ['created_at_detailed', 'served_at_detailed']
    ordering = ['-created_at']

    def round_uuid(self, obj: Drinker):
        return str(obj.round.uuid)

    def created_at_detailed(self, obj: Drinker):
        return obj.created_at.strftime("%d %b %Y %H:%M:%S.%f")[:-3]

    def served_at_detailed(self, obj: Drinker):
        if obj.served_at is None:
            return None
        return obj.served_at.strftime("%d %b %Y %H:%M:%S.%f")[:-3]

    def waited_seconds(self, obj: Drinker):
        if obj.served_at is None:
            return None

        return f"{(obj.served_at - obj.created_at).seconds}s"

    round_uuid.short_description = 'Round'

    created_at_detailed.short_description = 'Created at'
    created_at_detailed.admin_order_field = '-created_at'

    served_at_detailed.short_description = 'Served at'
    served_at_detailed.admin_order_field = '-served_at'
