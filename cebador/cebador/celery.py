from enum import Enum, unique

from celery import Celery

app = Celery('cebador')
app.config_from_object("django.conf:settings", namespace="CELERY")
app.autodiscover_tasks()


@unique
class Queues(Enum):
    fast_drinker = "fast-drinker"
    slow_drinker = "slow-drinker"
